# coolAdvancedUnityProject

This an open-world rpg simulation/game unity project for the Advanced Unity Course @UOS.

**Development**| Members
---------------|--------------------
Overall Prototype Development	| Vera Klütz, Michael Rau, Frederik Wollatz, Qirui Zhu 
Overall Git Management	|	Qirui Zhu 
Forest Level Design		|				Michael Rau 
Urban City Level Design		| Vera Klütz
SpaceStation Level Design		|		Frederik Wollatz
Game Backbone Development		|	  Qirui Zhu

## Used Assets

**Assets Overall** | -
---------------|-------------
Player Model			|						  Space Robot Kyle ([Unity Store](https://assetstore.unity.com/packages/3d/characters/robots/space-robot-kyle-4696))
Player Animations	|					  Mixamo.com ([3rd party](https://www.mixamo.com/#/?page=1&query=&type=Motion%2CMotionPack))
Camera 						|					    Cinemachine (Unity in-built package)
Gems							|					    Sets - Gems ([Unity Store](https://assetstore.unity.com/packages/3d/props/sets-gems-19902))
Crates						|					  Cartoon_crate_collection ([Unity Store](https://assetstore.unity.com/packages/3d/props/cartoon-crate-collection-2550))

**Assets Urban City Level**| -
---------------|-------------
Power Poll Particle Effects	|		ParticleLight ([Unity Store](https://assetstore.unity.com/packages/vfx/shaders/particle-light-10105))
Power Poll Lightning Bolt		|		LightningBolt ([Unity Store](https://assetstore.unity.com/packages/tools/particles-effects/lightning-bolt-effect-for-unity-59471))
Building & Surroundings			|		POLYGON city pack ([Unity Store](https://assetstore.unity.com/packages/3d/environments/urban/city-package-107224))
Cars												|	  carAssets ([Unity Store](https://assetstore.unity.com/packages/3d/vehicles/land/simple-cars-pack-97669))

**Assets SpaceStation Level**| -
---------------|-------------
Overall Level Design				|		Probuilder (Unity in-built package)
Space Shuttles							|		ArvisMag_Assets ([Unity Store](https://assetstore.unity.com/packages/3d/vehicles/space/space-shuttle-34972))
Astroids										|	  Asteroids pack ([Unity Store](https://assetstore.unity.com/packages/3d/environments/sci-fi/asteroids-low-poly-pack-142164))
Background and Sky					|	  ColorSkies ([Unity Store](https://assetstore.unity.com/packages/2d/textures-materials/sky/colorskies-91541)) + DeepSpaceSkyboxPack ([Unity Store](https://assetstore.unity.com/packages/2d/textures-materials/deep-space-skybox-pack-11056))
Solar Panels								|	  SolarPanel ([free3d.com](https://free3d.com/3d-model/solar-panels-v1--376601.html))

**Assets Forest Level**| -
---------------|-------------
Fences											|	  Assets_FenceChained ([Unity Store](https://assetstore.unity.com/packages/3d/chainlink-fences-73107))
Forest House								|	  Survival Old House by Nikolay Fedorov ([Unity Store](https://assetstore.unity.com/packages/3d/environments/urban/survival-old-house-55315))
Old Tower										|    Round Tower ([Unity Store](https://assetstore.unity.com/packages/3d/environments/historic/round-tower-5581))
Forest Tools (Axes, ...)		|		ToolsAndLogsSAMPLE ([Unity Store](https://assetstore.unity.com/packages/3d/props/tools-and-logs-43971))
Wooden Crates								|    WoodCrate ([Unity Store](https://assetstore.unity.com/packages/3d/props/wood-crate-4428))
Base Level Design						|	  Polytope Studio ([Unity Store](https://assetstore.unity.com/packages/3d/environments/lowpoly-environment-nature-pack-free-187052))
Ruins                       |   Altar Ruins Free ([Unity Store](https://assetstore.unity.com/packages/3d/environments/fantasy/altar-ruins-free-109065))
