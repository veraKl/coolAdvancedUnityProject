﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTriggers : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            String triggerName = this.gameObject.name;
            Debug.Log("Player entered Trigger!");
            if (triggerName.Contains("Start"))
            {
                TutorialManager.Instance.SetWelcomeText();
            }else if (triggerName.Contains("BasicMovement"))
            {
                TutorialManager.Instance.SetBasicMovementText();
            }else if (triggerName.Contains("Collectable"))
            {
                TutorialManager.Instance.SetCollectablesText();
            }else if (triggerName.Contains("Cam"))
            {
                TutorialManager.Instance.SetCamText();
            }else if (triggerName.Contains("Jump"))
            {
                TutorialManager.Instance.SetJumpText();
            }else if (triggerName.Contains("Sliding"))
            {
                TutorialManager.Instance.SetSlidingText();
            }else if (triggerName.Contains("Climbing"))
            {
                TutorialManager.Instance.SetClimbingText();
            }
            else if (triggerName.Contains("Grapple"))
            {
                TutorialManager.Instance.SetGrapplingText();
            }else if (triggerName.Contains("Compass"))
            {
                TutorialManager.Instance.SetCompassText();
            }else if (triggerName.Contains("DeathAndRespawn"))
            {
                TutorialManager.Instance.SetDeathAndRespawnText();
            }else if (triggerName.Contains("Finish"))
            {
                TutorialManager.Instance.SetFinishText();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            TutorialManager.Instance.DisableTextBox();
        }
    }
}
