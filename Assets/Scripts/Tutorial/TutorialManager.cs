﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{

    [SerializeField]
    private Text _textBox;

    [SerializeField] 
    private Compass _compass;

    //Singleton
    private static TutorialManager _instance;
    
    #region Region: Texts

    private const String _welcomeText = "Welcome to the Tutorial level. This level will get you familiar with the controls and mechanics of our game!";
    
    private const String _collectableText = "Collect Gems to increase your points. Different Gem types will give different points. \n" +
                                            "Collect enough points to finish a level.";

    private const String _basicMovementText = "Basic Movement uses the W A S D keys: \n" +
                                              "W   Move forward  \n" +
                                              "A   Move left \n" +
                                              "S   Move backward \n" +
                                              "D   Move right \n" +
                                              "You can look up all information under the 'How to play' section in the pause menue (ESC Key).";

    private const String _jumpText =
        "Use SPACE to jump. You can jump up to 2 times without touching the ground again. \n" +
        "Try to jump up the crates to proceed within the level.";
    
    private const String _camMovementText = "Camera Movement is bind to the mouse. Use the settings menu to adjust the sensivity for the X- and Y-axis. \n" +
                                            "Use the F key to recalibrate the camera. Use the scroll wheel to zoom in and out.";

    private const String _slidingText = "Use the left CTRL Key to slide under obstacles while running.";

    private const String _climbingText =
        "Press W infront a climbable object (in this level marked in green) to climb upwards.\n" +
        "Use the A and D to move vertically while climbing. Make sure build enough momentum to climb over the edge on top of the object.";
    
    private const String _grapplingText = "Available (in range) Grapple Points are shown in red color. Hold down the left mouse key to grapple one of the points.\n" +
                                          "Release the left mouse key again to stop grappling. \n" +
                                          "Tips: Jumping is allowed during grappling. Gain Speed and jump before grappling for a good swing!";

    private const String _compassText = "The compass above shows the direction of the final target of a level. Reach the location with enough points to finish a level!";

    private const String _deathAndRespawnText =
        "This is the final room. Below the platform is the deathzone. Falling into the deathzone will respawn you at your last Respawn Checkpoints.\n" +
        "Checkpoints are round circles on the ground and will be set automatically when stepped on it. Falling into the deathzone can sometimes remove points!\n" +
        "In case you are stuck, press R to respawn manually at the latest Checkpoint.";
    
    private const String _finishText = "This is the finish point of a level. You can complete the level with enough points.";
    
    
    #endregion
    
    #region Region: Public Properties

    public static TutorialManager Instance
    {
        get { return _instance; }
    }
    
    #endregion

    public void Awake()
    {
        //Init Singleton Instance
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void SetWelcomeText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _welcomeText;
    }
    
    
    public void SetCollectablesText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _collectableText;
    }
    
    public void SetBasicMovementText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _basicMovementText;
    }
    
    public void SetJumpText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _jumpText;
    }

    public void SetCamText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _camMovementText;
    }
    
    public void SetSlidingText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _slidingText;
    }

    public void SetClimbingText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _climbingText;
    }
    
    public void SetGrapplingText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _grapplingText;
    }
    
    public void SetCompassText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _compassText;
        _compass.gameObject.SetActive(true);
    }
    
    public void SetDeathAndRespawnText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _deathAndRespawnText;
    }
    
    public void SetFinishText()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = _finishText;
    }

    public void DisableTextBox()
    {
        _textBox.gameObject.SetActive(false);
    }
}
