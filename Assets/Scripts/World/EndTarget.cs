﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndTarget : MonoBehaviour
{
    [SerializeField] private int _pointsNeededToFinish;

    [SerializeField] private SceneManager.SceneNumbers _nextScene;
   

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.PlayerParent.Health.Points >= _pointsNeededToFinish)
            {
                CrossSceneManager.Instance.SceneManager.LoadNextLevel(_nextScene);
            }
            else
            {
                StartCoroutine(GameManager.Instance.UIManager.NotEnoughPointsNotification(_pointsNeededToFinish));
            }
        }
    }
}
