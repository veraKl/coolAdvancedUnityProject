﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;
using UnityEngine.UI;

public class SpawnPoint : MonoBehaviour
{
    //set the players current spawnpoint to this spawnpoint!
    private bool _isCurrent = false;
    
    [SerializeField] 
    private Text _textBox;

    private void Start()
    {
        if (this.CompareTag("StartPoint"))
        {
            StartCoroutine(this.StartLevelAnnouncement());
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        
        //updating the spawnpoint;
        if (other.CompareTag("Player"))
        {
            _isCurrent = true;
            other.GetComponent<Respawn>().SetSpawnpoint(this);
            StartCoroutine(this.SpawnSetFeedback());
        }
    }

    //setter for is Current
    public void SetIsCurrent(Boolean b)
    {
        _isCurrent = b;
    }

    private IEnumerator SpawnSetFeedback()
    {
        if (!this.CompareTag("StartPoint"))
        {
            _textBox.gameObject.SetActive(true);
            _textBox.text = "New Spawnpoint has been set!";
            yield return new WaitForSeconds(5f);
            _textBox.gameObject.SetActive(false);
        }
    }

    private IEnumerator StartLevelAnnouncement()
    {
        _textBox.gameObject.SetActive(true);
        _textBox.text = String.Format("Try to find the final end target using the compass and collect enough points on the way! \n" +
                                      "Be careful some objects will cause damage and reduce your points!");
        yield return new WaitForSeconds(10f);
        _textBox.gameObject.SetActive(false);
    }
}
