﻿using System;
using UnityEngine;

namespace World
{
    public class SkeltalMove : MonoBehaviour
    {
        private float speed = 1f;
        private CharacterController _controller;
        private Transform _target; //This will be the player
        private Transform _skeltalTransform;
        private float _chaseSpeed = 1.5f; //speed when chasing player
        //private float _minDistance = 1f;
        private float _pushForce = 1500f;
        [SerializeField] private Animator _skeltalAnimator;
        [SerializeField] private LayerMask _groundLayer;
        private static readonly int OnAttack = Animator.StringToHash("OnAttack");

        // Start is called before the first frame update
        void Start()
        {
            _controller = GetComponent<CharacterController>();
        }

        // Update is called once every five frames
        void FixedUpdate()
        {
            FindTarget();
        }

        private void FindTarget()
        {
            float targetRange = 20f;
            _skeltalTransform = transform;
            _target = GameObject.FindGameObjectWithTag("Player").transform;
            //_target = GameManager.Instance.PlayerParent.Player.transform; //target the player
            Vector3 forward = transform.TransformDirection(Vector3.forward);
            float distance = Vector3.Distance(_skeltalTransform.position, _target.position);
            //Debug.Log("Distance:" + distance);


            if (distance < targetRange)
            {
                // Look at player and chase!
                _skeltalTransform.LookAt(_target);
                _controller.SimpleMove(forward * _chaseSpeed);
                
            }
            else
            {
                // Patrol in circles when the player is far away
                _controller.SimpleMove(forward * speed);
                //_skeltalBody.AddForce(transform.forward * speed,ForceMode.Force);
                transform.Rotate(0, 0.5f, 0);
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.transform.CompareTag("Player"))
            {
                Debug.Log("Skeltalattack!");
                _skeltalAnimator.SetTrigger("OnAttack");
                GameManager.Instance.PlayerParent.Movement.PlayerBody.AddForce(-GameManager.Instance.PlayerParent.Movement.transform.forward * _pushForce,ForceMode.Impulse);
                GameManager.Instance.PlayerParent.Health.RemovePoints(10);
            }
        }
    }
}
