﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using Random = UnityEngine.Random;

public class LightningOnOff : MonoBehaviour
{
    // Start is called before the first frame update
    private new LineRenderer _lr;
    private new CapsuleCollider _collider;
    [SerializeField] private new Boolean _isActive;
    private new Vector3 startingPoint;
    [SerializeField] private float waitingTime;
    [SerializeField] private float offset; //time by which the shutdown/start is moved  

    void Start()
    {
        _lr = this.GetComponent<LineRenderer>();
        _collider = GetComponent<CapsuleCollider>();
        startingPoint = this.transform.position;
        //
        Invoke("go", offset);
    }
    
    void go()
    {
        StartCoroutine(OnOff());
    }

        IEnumerator OnOff() { 
            while (true)
                {
                    if (_isActive)
                    {
                        this.transform.position = new Vector3(100, 100, 100);
                    }
                    else
                    {
                        this.transform.position = startingPoint;
                    }

                    //inverting activity
                    _isActive = !_isActive;

                    yield return new WaitForSeconds(waitingTime);
                }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                //push the player in a random dirction when hit! The higher "Power" the higher the force
                int power = 10;
                other.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f))*power,ForceMode.Impulse);
            }
        }
}
