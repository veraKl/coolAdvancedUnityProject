﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Accelerator : MonoBehaviour
{
    [SerializeField] private float _force;
    [SerializeField] private ForceMode _forceMode;
    [SerializeField] private bool OnStay;
    public void OnTriggerEnter(Collider other)
    {
        if (!OnStay)
        {
            if (other.CompareTag("Player"))
            {
                Rigidbody _rb = other.GetComponent<Rigidbody>();
                //Change the Force, depending on Mass!!!
                _rb.AddForce(this.transform.up*(_force*_rb.mass), _forceMode);
            }
        }
        
    }

    public void OnTriggerStay(Collider other)
    {
        if (OnStay)
        {
            if (other.CompareTag("Player"))
            {
                Rigidbody _rb = other.GetComponent<Rigidbody>();
                //Change the Force, depending on Mass!!!
                _rb.AddForce(this.transform.up*(_force*_rb.mass), _forceMode);
            }
        }
        
    }
}
