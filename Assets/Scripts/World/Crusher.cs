﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crusher : MonoBehaviour
{
    private Rigidbody _crusherBody;
    // Start is called before the first frame update
    void Start()
    {
        _crusherBody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        _crusherBody.AddForce(Vector3.down);
    }
    
}
