﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointCollectable : MonoBehaviour
{
    [SerializeField] private int _points;
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag("Player"))
        {
            Debug.Log("COLLISION DETECTED");
            GameManager.Instance.PlayerParent.Health.AddPoints(_points);
            GameObject.Destroy(this.gameObject);
        }
    }
}
