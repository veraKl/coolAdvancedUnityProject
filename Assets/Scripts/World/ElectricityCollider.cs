﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricityCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // if player touches electricity
        if (other.transform.CompareTag("Player"))
        {
            GameManager.Instance.PlayerParent.Health.RemovePoints(20);
            for (int i = 0; i < 180; i++) 
            {
                other.transform.Translate(Vector3.right * Time.deltaTime);
            }
        }
    }
}
