﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCar : MonoBehaviour
{
    private float _speed = 10f;
    private bool _isInsideTurnPoint = false;

    // Start is called before the first frame update
    void Start()
    {
        //this.transform.position = new Vector3(212.5f,0.6f,-3.13f);
        //this.transform.position = new Vector3(252.97f,0.6f,-15.6f);
        //this.transform.position = new Vector3(249.1f, 0.6f, 11f);
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Translate(Time.deltaTime * _speed * Vector3.right);

        if ((this.transform.position.x > 500) || (this.transform.position.z > 200) ||
            (this.transform.position.x < -100) || (this.transform.position.z < -300))
        {
            this.transform.position = new Vector3(212.5f,0.9f,-3.13f);
        }
    }
    
    private void OnTriggerEnter(Collider other)
    {
        // if car collides with player
        if (other.transform.CompareTag("Player"))
        {
            GameManager.Instance.PlayerParent.Health.AddPoints(-5);
            int _randomDir = Random.Range(1, 5);
            for (int i = 0; i < 80; i++)
            {
                other.transform.Translate(Vector3.up * Time.deltaTime);
                switch (_randomDir)
                {
                    case 1:
                        other.transform.Translate(Vector3.left * Time.deltaTime);
                        break;
                    case 2:
                        other.transform.Translate(Vector3.right * Time.deltaTime);
                        break;
                    case 3:
                        other.transform.Translate(Vector3.forward * Time.deltaTime);
                        break;
                    case 4:
                        other.transform.Translate(Vector3.back * Time.deltaTime);
                        break;
                }
            }
        }
        
    
        //if car collides with an invisble wall placed at all turnpoints (crossroads)
        if ((other.gameObject.name.Contains("TurnCarRbigStreet")) || (other.gameObject.name.Contains("TurnCarLeftBigStreet")) || (other.gameObject.name.Contains("TurnCarRightFrontBigStreet")) || (other.gameObject.name.Contains("TurnCarLeftFrontBigStreet")))
        {
            if (_isInsideTurnPoint)  // is car currently inside turn point (crossroad)?
            {
                _isInsideTurnPoint = false;   // leave crossroad without turning again
            }
            else  //if car enters crossroad
            {
                int turnRightDistance = 0;
                int goStraightDistance = 0;
                int turnLeftDistance = 0;
                if (other.gameObject.name.Contains("TurnCarRbigStreet")) //depending on which direction the car enters the crossroad, the distances for turning right have to be adjusted
                {
                    turnRightDistance = 270;  
                    goStraightDistance = 95;
                    turnLeftDistance = 460;
                }
                else if(other.gameObject.name.Contains("TurnCarLeftBigStreet"))
                {
                    turnRightDistance = 365;
                    goStraightDistance = 95;
                    turnLeftDistance = 560;
                }
                else if(other.gameObject.name.Contains("TurnCarRightFrontBigStreet"))
                {
                    turnRightDistance = 265;  
                    goStraightDistance = -95;
                    turnLeftDistance = 460;
                }
                else // TurnCarLeftFrontBigStreet
                {
                    turnRightDistance = 365;
                    goStraightDistance = -95;
                    turnLeftDistance = 560;
                }
                
                int randomDirection = Random.Range(1, 4);  //1 = turn right, 2 = go straight, 3 = turn left
                _isInsideTurnPoint = true;
                //randomDirection = 1;
                
                switch (randomDirection)
                {
                    case 1: // turn right
                        for (int distance = 0; distance < turnRightDistance; distance++)
                        {
                            this.transform.Translate(Vector3.right * Time.deltaTime); 
                        }

                        this.transform.Rotate(0, 90, 0);
                        break;

                    case 2: // go straight
                        for (int distance = 0; distance < 1200; distance++) //drive into the middle of the crossroad
                        {
                            this.transform.Translate(Vector3.right * Time.deltaTime * 0.25f);
                        }

                        for (int distance2 = 0; distance2 < Mathf.Abs(goStraightDistance); distance2++)  //depending from where you come, move the car to the left or right
                        {
                            if (goStraightDistance > 0)  
                            {
                                this.transform.Translate(Vector3.forward * Time.deltaTime); // move the car to the left
                            }
                            else
                            {
                                this.transform.Translate(Vector3.back * Time.deltaTime); // move the car to the right
                            }
                            
                        }

                        break;

                    case 3: // turn left
                        for (int distance = 0; distance < turnLeftDistance; distance++)
                        {
                            this.transform.Translate(Vector3.right * Time.deltaTime); 
                        }

                        this.transform.Rotate(0, -90, 0);
                        break;

                    default: // if anything goes wrong, go on driving
                        break;
                }
            }
        }
        
        
        // if car collides with invisible wall placed at all dead end streets
        else if (other.gameObject.name.Contains("deadEndStreet"))
        {
            this.transform.Rotate(0,180,0);
            this.transform.Translate(Vector3.back * Time.deltaTime * 95); // move the car to the right
        }
        
        
        // if car is on a small street and can go left or right or straight or multiple of these
        else if ((other.gameObject.name.Contains("TurnCarRightSmallStreet")) || (other.gameObject.name.Contains("TurnCarLeftSmallStreet") || (other.gameObject.name.Contains("TurnCarRightOrStraightSmallStreet")) || (other.gameObject.name.Contains("TurnCarLeftOrRightSmallStreet")) || (other.gameObject.name.Contains("TurnCarLeftOrStraightSmallStreet"))))
        {
            if (_isInsideTurnPoint)  // is car currently inside turn point (crossroad)?
            {
                _isInsideTurnPoint = false;   // leave crossroad without turning again
            }
            else //if car enters crossroad
            {
                _isInsideTurnPoint = true;
                int _direction = 1;  //1 = turn right, 2 = go straight, 3 = turn left
                
                // choose direction, in which the car should go
                if (other.gameObject.name.Contains("TurnCarLeftSmallStreet"))
                {
                    _direction = 3;
                }
                else if (other.gameObject.name.Contains("TurnCarRightOrStraightSmallStreet"))
                {
                    _direction = Random.Range(1, 3);
                }
                else if (other.gameObject.name.Contains("TurnCarLeftOrRightSmallStreet"))
                {
                    int[] arrayOfOptions = {1,3};
                    _direction = arrayOfOptions[Random.Range(0,2)];
                }
                else if (other.gameObject.name.Contains("TurnCarLeftOrStraightSmallStreet"))
                {
                    _direction = Random.Range(2, 4);
                }
                
                // turn car if necessary
                if (_direction == 1) // turn right
                {
                    for (int distance = 0; distance < 270; distance++)
                    {
                        this.transform.Translate(Vector3.right * Time.deltaTime); 
                    }
                    this.transform.Rotate(0, 90, 0);
                }
                else if (_direction == 3) // turn left
                {
                    for (int distance = 0; distance < 375; distance++)
                    {
                        this.transform.Translate(Vector3.right * Time.deltaTime); 
                    }
                    this.transform.Rotate(0, -90, 0);
                }
            }
        }
        
    }
}
