﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Asteroid_Emitter : MonoBehaviour
{
    private Transform asteroid;
    private Rigidbody asteroid_rb;
    private Vector3 asteroidPos;
    private Vector3 emitterPos;
    private Quaternion asteroidRot;
    [SerializeField] private int force = 20;
    private Animation anim;


    public void Start()
    {
        //gets the asteroid
        asteroid = transform.GetChild(0);
        asteroid_rb = asteroid.GetComponent<Rigidbody>();
        asteroidRot = asteroid.transform.rotation;
        emitterPos = transform.position;
        anim = asteroid.GetComponent<Animation>();
        accelerate();
    }

    void Update()
    {
        //update position of asteroid
        this.asteroidPos = asteroid.transform.position;

        // when the distance is far enough: return it home!
        //Debug.Log(Vector3.Distance(asteroidPos,emitterPos));
        if (Vector3.Distance(asteroidPos,emitterPos) > 50)
        {
            if (!anim.isPlaying)
            {
                anim.Play("shrink");
            }
            else
            {
                transform.position = emitterPos;
                asteroid.transform.position = emitterPos;
                accelerate();
            }
        }
    }

    private void accelerate()
    {
        //resetting and restarting the course of the meteor
        anim.Play("grow");
        asteroid_rb.velocity = new Vector3(0, 0, 0);
        asteroid_rb.angularVelocity = new Vector3(0, 0, 0);
        asteroid.transform.rotation = asteroidRot;
        asteroid_rb.AddForce(transform.up * (force * asteroid_rb.mass), ForceMode.Impulse);
    }

}
