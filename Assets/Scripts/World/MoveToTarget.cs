﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget: MonoBehaviour
{
    public Transform[] target;
    public float speed;

    private int _current;
    
    
    private Rigidbody _body;
    // Start is called before the first frame update
    void Start()
    {
        //get the rigidbody 
        _body = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //when the distance between current location and target is small enough: Choose the next target!
        if (Vector3.Distance(transform.position ,target[_current].position)>=0.5)
        {
            Vector3 pos = Vector3.MoveTowards(transform.position, target[_current].position, speed * Time.deltaTime);
            _body.MovePosition(pos);
        }
        else
        {
            //choose the next target
            _current = (_current + 1) % target.Length;
        }
    }

}
