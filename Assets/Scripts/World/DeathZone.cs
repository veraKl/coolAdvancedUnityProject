﻿using System;
using System.Collections;
using System.Collections.Generic;
using Player;
using UnityEngine;

public class DeathZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        //let the player Respawn
        //other.GetComponent<Respawn>().PlayerRespawn();
        if (other.CompareTag("Player") && other.gameObject.name != "GrappleSphere")
        {
            GameManager.Instance.PlayerParent.Respawn.PlayerRespawn();
        }

        
    }
}
