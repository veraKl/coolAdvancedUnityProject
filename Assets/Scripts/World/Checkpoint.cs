﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Checkpoint : MonoBehaviour
{
    [SerializeField]
    private Text _textbox;
    [SerializeField]
    private Transform _nextNavigationPoint;
    [SerializeField]
    private Compass _compass;

    [SerializeField] 
    private Transform _endTarget;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _textbox.gameObject.SetActive(true);
            _textbox.text = "Next Navigation Point has been set! Follow the compass to reach the new target!";
            if (_nextNavigationPoint != null)
            {
                //In case the next Checkpoint has been already destroyed (Player has diverged from the planned route)
                _compass.SetNewTarget(_nextNavigationPoint);
            }
            else
            {
                _compass.SetNewTarget(_endTarget);
            }
                
            //Destroy after 5 seconds
            GameObject.Destroy(this.gameObject,5f);
        }
    }

    private void OnDestroy()
    {
        if(_textbox != null)
            _textbox.gameObject.SetActive(false);
    }
}
