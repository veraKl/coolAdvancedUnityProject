﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    private Rigidbody _rb;
    private bool start;
    private void Start()
    {
        _rb=GetComponent<Rigidbody>();
        Invoke("remove", 3);
        start = true;
    }

    private void FixedUpdate()
    {
        //checks weather the projectile is out of its initial slow phase.
        if (_rb.velocity.magnitude > 10)
        {
            start = false;
        }

        //if its too slow: delete it!
        if(_rb.velocity.magnitude<10 && !start){
            remove();
        }
    }

    private void remove()
    {
        Destroy(this.gameObject);
    }
    
    
    
}
