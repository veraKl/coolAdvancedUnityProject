﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Turret : MonoBehaviour
{
    //based on https://forum.unity.com/threads/unity-turret-tutorial.341866/ , accessed 27.09.21
    public float speed = 3.0f;

    [SerializeField] GameObject m_target = null;
    [SerializeField] private GameObject projectile;
    [SerializeField] private GameObject muendung;
    [SerializeField] private int force;
    [SerializeField] private int radius;
    Vector3 m_lastKnownPosition = Vector3.zero;

    Quaternion m_lookAtRotation;

    //the layer that the player is on
    [SerializeField] private LayerMask _playerLayer;

    private void Start()
    {
        StartCoroutine(ShootCountdown());
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.CheckSphere(transform.position, radius, _playerLayer)) //checks weather the player is close enough 
        {
            if (m_target)
            {
                if (m_lastKnownPosition != m_target.transform.position)
                {
                    m_lastKnownPosition = m_target.transform.position;
                    m_lookAtRotation =
                        Quaternion.LookRotation(m_lastKnownPosition + new Vector3(0, 1, 0) - transform.position);
                }

                if (transform.rotation != m_lookAtRotation)
                {
                    transform.rotation =
                        Quaternion.RotateTowards(transform.rotation, m_lookAtRotation, speed * Time.deltaTime);
                }
            }
        }
    }

    public void Shoot()
    {
        
        
        var rayDirection = m_lastKnownPosition - transform.position;
        RaycastHit hit;
        if (Physics.Raycast(transform.position+new Vector3(0,1,0), rayDirection, out hit, radius))
        {
            if (hit.transform.CompareTag("Player"))
            {
                // enemy can see the player!

                if (Physics.CheckSphere(transform.position, radius, _playerLayer))
                {
                    //create the projectile
                    Vector3 MündungsPos = muendung.transform.position;
                    Quaternion MündungsRot = muendung.transform.rotation;
                    GameObject proj = Instantiate(projectile, MündungsPos, MündungsRot);
                    Vector3 direction = m_lastKnownPosition - MündungsPos;
                    //fire the projectile
                    proj.GetComponent<Rigidbody>().AddForce(direction * force, ForceMode.Impulse);
                }
            }
            else
            {
                // there is something obstructing the view
            }
        }
    }

    IEnumerator ShootCountdown()
    {
        while (true)
        {
            Shoot();
            yield return new WaitForSeconds(2f);
        }
    }
    
    private void OnDrawGizmosSelected()
    {   //vizuallizes if the playewr is grounded.
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, radius);
    }
}
