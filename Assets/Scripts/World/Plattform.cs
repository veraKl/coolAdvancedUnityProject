﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Plattform : MonoBehaviour
{

    //Plattform-Parent
    [SerializeField] private GameObject plattformParent;

    private void OnTriggerEnter(Collider other)
    {
        //if the object is a player, use the plattformparent as a parent for the player as well, so it moves with me.
        if (other.CompareTag("Player"))
        {
            Debug.Log("Player is on Platform");
            other.transform.SetParent(this.transform);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //reset the players parent, when the player leaves
        if (other.CompareTag("Player"))
        {
            Debug.Log("Player left Platform");
            other.transform.parent = null;
        }
    }
}