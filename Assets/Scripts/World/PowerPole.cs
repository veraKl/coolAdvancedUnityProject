﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerPole : MonoBehaviour
{
    
    [SerializeField] private bool _autoElectricity = false;
    [SerializeField] private float _onTime = 5f;
    [SerializeField] private float _offTime = 3f;
    [SerializeField] private bool _manualOnElectricity = true;
    private GameObject Electricity;
    private float _starttime;
    private float _currentTime;

    private IEnumerator AutoElectricity;
    // Start is called before the first frame update
    void Start()
    {
        Electricity = GetChildWithName(this.gameObject, "Electricity");
        if (Electricity == null)
        {
            Debug.Log("Electricity not found!");
        }
        AutoElectricity = AutoElectric(_onTime, _offTime);
        _starttime = Time.time;
        StartCoroutine(AutoElectricity);
    }

    // Update is called once per frame
    void Update()
    {
        if (_autoElectricity == true)
        {
            _manualOnElectricity = false;
        }
        if (_manualOnElectricity == true)
        {
            _autoElectricity = false;
            Electricity.SetActive(true);
        }
        else if (_autoElectricity==false)
        {
            Electricity.SetActive(false);
        }
    }
    
    private IEnumerator AutoElectric(float onTime, float offTime)
    {
        while (this.gameObject.activeSelf)
        {
            if (_autoElectricity)
            {
                _currentTime = Time.time;
                if ((Electricity.activeSelf) && ((_currentTime - _starttime) > onTime))
                {
                    Electricity.SetActive(false);
                    _starttime = _currentTime;
                }

                if (((Electricity.activeSelf) == false) && ((_currentTime - _starttime) > offTime))
                {
                    Electricity.SetActive(true);
                    _starttime = _currentTime;
                }
            }
            yield return null;
        }
        Debug.Log("Error: PowerPole not active anymore");
    }
    
    GameObject GetChildWithName(GameObject obj, string name) {
        Transform trans = obj.transform;
        Transform childTrans = trans. Find(name);
        if (childTrans != null) {
            return childTrans.gameObject;
        } else {
            return null;
        }
    }
}
