﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Streets : MonoBehaviour
{
    [SerializeField] private PhysicMaterial myMaterial;
    // Start is called before the first frame update
    void Start()
    {
        foreach (var child in GetAllChildren())
        {
            try
            {
                MeshCollider mesh = child.GetComponent<MeshCollider>();
                mesh.material = myMaterial;
            }
            catch 
            {
                // do nothing
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    List<GameObject> GetAllChildren()
    {
        List<GameObject> ListAllChildren = new List<GameObject>();

        Transform[] allChildren = GetComponentsInChildren<Transform>();
        foreach (Transform child in allChildren)
        {
            ListAllChildren.Add(child.gameObject);
        }
        return ListAllChildren;
    }
}
