﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndSceneUIManager : MonoBehaviour
{
    [SerializeField] private Text _finishText;
    // Start is called before the first frame update
    
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        if (CrossSceneManager.Instance.IsGameOver)
        {
            _finishText.text = String.Format("Game Over! No points left! \n" +
                                             "Try to collect more points next time!");;
        }
        else
        {
            _finishText.text = String.Format("Congratulations, You bet the game! \n" +
                                              "Your score: {0}",CrossSceneManager.Instance.OverallScore);
        }
    }

    public void BackToStartMenu()
    {
        CrossSceneManager.Instance.ResetGame();
        CrossSceneManager.Instance.SceneManager.LoadStartScene();
    }

    public void QuitGame()
    {
        CrossSceneManager.Instance.QuitGame();
    }
}
