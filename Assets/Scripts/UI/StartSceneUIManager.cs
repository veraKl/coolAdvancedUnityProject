﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartSceneUIManager : MonoBehaviour
{
    #region Region: Private Properties
    [SerializeField] private GameObject _startMenuPanel;
    [SerializeField] private GameObject _creditsPanel;
    [SerializeField] private GameObject _howToPlayPanel;
    [SerializeField] private GameObject _tutorialPanel;
    #endregion

    public void BackToMainMenu()
    {
        _creditsPanel.SetActive(false);
        _howToPlayPanel.SetActive(false);
        _startMenuPanel.SetActive(true);
    }

    public void ShowHowToPlayPanel()
    {
        _startMenuPanel.SetActive(false);
        _howToPlayPanel.SetActive(true);
    }

    public void ShowCreditsPanel()
    {
        _startMenuPanel.SetActive(false);
        _creditsPanel.SetActive(true);
    }

    public void ShowTutorialPanel()
    {
        _startMenuPanel.SetActive(false);
        _tutorialPanel.SetActive(true);
    }

    public void StartTutorial()
    {
        CrossSceneManager.Instance.SceneManager.LoadTutorialScene();
    }

    public void StartGame()
    {
        CrossSceneManager.Instance.SceneManager.LoadUrbanCityScene();
    }
}
