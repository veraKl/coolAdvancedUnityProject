﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    #region Region: Private Properties
    private static UIManager _instance;
    
    private PauseManager _pauseManager;

    [SerializeField]
    private GameObject _pauseMenuPanel;

    [SerializeField] 
    private GameObject _activeGameGUI;

    [SerializeField]
    private Text _textbox;
   

    [SerializeField] 
    private Text _timeLabelText;
    [SerializeField] 
    private Text _coinsLabeltext;

    [SerializeField] 
    private GameObject _howToPlayPanel;
    #endregion
    
    #region Region: Public Properties

    public bool Escapable { get; set;} = true;

    public static UIManager Instance()
    {
        return _instance;
    }

    #endregion

    private void Awake()
    {
        //Init Singleton Instance
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        if (_pauseManager == null)
        {
            _pauseManager = GameManager.Instance.PauseManager;
        }

        _timeLabelText.text = String.Format("Time: {0}", 0.00);
        _coinsLabeltext.text = String.Format("Coins: {0}", 0);
    }

    private void Update()
    {
        if (_pauseManager == null)
        {
            _pauseManager = GameManager.Instance.PauseManager;
        }
        
        //Todo: Make Game unpausable during grappling
        if (Input.GetKeyDown(KeyCode.Escape) && this.Escapable)
        {   
            //if game is already paused, resume
            if (PauseManager.GameIsPaused)
            {
                _pauseMenuPanel.SetActive(false);
                _activeGameGUI.SetActive(true);
                _pauseManager.Resume();
            }
            else
            {
                _pauseMenuPanel.SetActive(true);
                _activeGameGUI.SetActive(false);
                _pauseManager.Pause();
            }
        }

        if ((int) Time.time < 60)
        {
            _timeLabelText.text = String.Format("Time: {0}s", (int)Time.time);
        }
        else
        {
            int min = (int)(Time.time / 60);

            _timeLabelText.text = String.Format("Time: {0} min {1}s", min, (int)(Time.time - min * 60));
        }
        
        _coinsLabeltext.text = String.Format("Coins: {0}", GameManager.Instance.PlayerParent.Health.Points);
    }

    public void ShowGameGui()
    {
        _activeGameGUI.SetActive(true);
    }

    public IEnumerator NotEnoughPointsNotification(int pointsNeeeded)
    {
        _textbox.gameObject.SetActive(true);
        _textbox.text = String.Format("Not enough points to finish! Collect {0} coins to finish.", pointsNeeeded);
        yield return new WaitForSeconds(5f);
        _textbox.gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        CrossSceneManager.Instance.QuitGame();
    }
}
