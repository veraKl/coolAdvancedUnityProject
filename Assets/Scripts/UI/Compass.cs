﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    public RawImage compassImage;
    public Transform camera;
    public Transform target;

    public void SetNewTarget(Transform newTargetTransform)
    {
        this.target = newTargetTransform;
    }
    
    
    
    // Update is called once per frame
    void Update()
    {
        //get the vectors from camera to target and from camera to point the player is looking at.
        Vector2 cameratotarget =
            new Vector2(target.position.x - camera.position.x, target.position.z - camera.position.z);
        Vector2 cameratoview = new Vector2(camera.transform.forward.x, camera.transform.forward.z);
        //compute the angle between the two vectors above
        float angle = Vector2.SignedAngle(cameratotarget, cameratoview);
        //transform to compass image
        compassImage.uvRect = new Rect(-angle/360,0f,1f,1f);
    }
    
}
