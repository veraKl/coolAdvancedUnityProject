﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour
{
    #region Region: Public Properties
    
    //globally accessible GamePause variable
    public static bool GameIsPaused = false;
    
    public static PauseManager Instance()
    {
        return _instance;
    }
    
    #endregion

    #region Region: Private Properties

    private static PauseManager _instance;

    [SerializeField] 
    private GameObject _pauseMenuPanel;

    [SerializeField] 
    private GameObject _howToPlayPanel;
    
    #region Region: Private Fields OptionsPanel

    [SerializeField] 
    private GameObject _optionsPanel;
    
    //Cam settings
    private CinemachineFreeLook _camBrain;
    
    [SerializeField] 
    private Slider _camSensXSlider;

    [SerializeField] 
    private Slider _camSensYSlider;
    
    #endregion

    #endregion
    
    

    void Awake()
    {
        //Init Singleton Instance
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        Cursor.lockState = CursorLockMode.Locked;
    }

    void Start()
    {
        if (_camBrain == null)
        {
            
            _camBrain = GameManager.Instance.PlayerParent?.Movement.CamBrain;
        }
    }
    
    public void Resume()
    {
        Time.timeScale = 1f;
        GameIsPaused = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        _pauseMenuPanel.SetActive(false);
        _optionsPanel.SetActive(false);
        GameManager.Instance.UIManager.ShowGameGui();
    }

    public void Pause()
    {
        //freeze time
        Time.timeScale = 0f;
        GameIsPaused = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
    }

    public void Quit()
    {
        CrossSceneManager.Instance.QuitGame();
    }

    public void LoadOptionsMenu()
    {
        _pauseMenuPanel.SetActive(false);
        _optionsPanel.SetActive(true);
        GameManager.Instance.UIManager.Escapable = false;
        //init cam sensivity settings
        if(_camBrain == null)
            _camBrain = GameManager.Instance.PlayerParent.Movement.CamBrain;
        _camSensXSlider.maxValue = 500f;
        _camSensXSlider.minValue = 0f;
        _camSensXSlider.value = _camBrain.m_XAxis.m_MaxSpeed;
        _camSensYSlider.maxValue = 2.5f;
        _camSensYSlider.minValue = 0f;
        _camSensYSlider.value = _camBrain.m_YAxis.m_MaxSpeed;
    }

    public void XSliderOnValueChanged()
    {
        _camBrain.m_XAxis.m_MaxSpeed = _camSensXSlider.value;
    }
    
    public void YSliderOnValueChanged()
    {
        _camBrain.m_YAxis.m_MaxSpeed = _camSensYSlider.value;
    }
    
    public void ResetToDefaultSettings()
    {
        //reset cam sense settings
        _camBrain.m_XAxis.m_MaxSpeed = CamDefaultSettings.SensivityX;
        _camBrain.m_YAxis.m_MaxSpeed = CamDefaultSettings.SensivityY;
        _camSensXSlider.value = _camBrain.m_XAxis.m_MaxSpeed;
        _camSensYSlider.value = _camBrain.m_YAxis.m_MaxSpeed;
    }
    
    public void ReturnToPauseMenu()
    {
        _optionsPanel.SetActive(false);
        _pauseMenuPanel.SetActive(true);
        _howToPlayPanel.SetActive(false);
        GameManager.Instance.UIManager.Escapable = true;
    }
    
    public void ShowHowToPlay()
    {
        GameManager.Instance.UIManager.Escapable = false;
        _pauseMenuPanel.SetActive(false);
        _howToPlayPanel.SetActive(true);
    }
}


