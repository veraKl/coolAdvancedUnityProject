﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour
{
    private static SceneManager _instance;

    public static SceneManager Instance
    {
        get{return _instance;}
    }
    
    public enum SceneNumbers
    {
        StartScene = 0,
        SampleScene = 1,
        EndScene = 2,
        TutorialScene = 3,
        UrbanCityScene = 4,
        SpaceStationScene = 5,
        ForestScene = 6
    }

    private void Awake()
    {
        //Init Singleton Instance
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void LoadSceneByBuiltIndex(int buildIndex)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(buildIndex, LoadSceneMode.Single);
    }
    
    public void LoadNextLevel(SceneNumbers Scene)
    {
        if (Scene == SceneNumbers.SpaceStationScene ||
            Scene == SceneNumbers.ForestScene)
        {
            CrossSceneManager.Instance.AddScore(GameManager.Instance.PlayerParent.Health.Points);
            GameManager.Instance.PlayerParent.Health.ResetPoints(100);
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene((int)Scene, LoadSceneMode.Single);
    }
    
    public void LoadStartScene()
    {
        LoadNextLevel(SceneNumbers.StartScene);
    }

    public void LoadSampleScene()
    {
        LoadNextLevel(SceneNumbers.SampleScene);
    }

    public void LoadEndScene()
    {
        LoadNextLevel(SceneNumbers.EndScene);
    }

    public void LoadTutorialScene()
    {
        LoadNextLevel(SceneNumbers.TutorialScene);
    }

    public void LoadUrbanCityScene()
    {
        LoadNextLevel(SceneNumbers.UrbanCityScene);
    }

    public void LoadSpaceStationScene()
    {
        LoadNextLevel(SceneNumbers.SpaceStationScene);
    }

    public void LoadForestScene()
    {
        LoadNextLevel(SceneNumbers.ForestScene);
    }

}


