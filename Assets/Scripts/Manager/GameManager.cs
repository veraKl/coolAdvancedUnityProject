﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    
    //UIManager
    private UIManager _uiManager;

    public UIManager UIManager
    {
        get { return _uiManager; }
    }

    //PauseManager 
    private PauseManager _pauseManager;

    public PauseManager PauseManager
    {
        get { return _pauseManager; }
    }
    //Player
    private PlayerParent _player;

    public PlayerParent PlayerParent
    {
        get{ return _player; }
    }
    
    
    //Singleton Instance
    private static GameManager _instance;

    //Getter for Singleton Instance
    public static GameManager Instance
    {
        get { return _instance; }
    }


    void Awake()
    {
        //Init Singleton Instance
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        CheckForManagerObjects();
    }

    void Start()
    {
    }
    void Update()
    {
        CheckForManagerObjects();
    }
    
    //event method called OnZeroHealthEvent
    public void OnGameOver()
    {
        Debug.Log("OnZeroHeatlhEventFired");
        CrossSceneManager.Instance.InitGameOver();
        CrossSceneManager.Instance.SceneManager.LoadEndScene();
    }

    private void CheckForManagerObjects()
    {
        if (_player == null)
            _player = new PlayerParent();
        if(_pauseManager == null)
            _pauseManager = PauseManager.Instance();
        if (_uiManager == null)
            _uiManager = UIManager.Instance();
    }
}
