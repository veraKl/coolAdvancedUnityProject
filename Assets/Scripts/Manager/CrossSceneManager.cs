﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossSceneManager : MonoBehaviour
{
    /*This is a Singleton Manager Instance which is maintained cross all scenes and
      does the cross scene information handling.*/

    private static CrossSceneManager _instance;
    private int _overallScore = 0;
    private bool _isGameOver = false;


    public bool IsGameOver => _isGameOver;
    public int OverallScore => _overallScore;

    public static CrossSceneManager Instance
    {
        get { return _instance; }
    }
    
    [SerializeField] private SceneManager _sceneManager;

    public SceneManager SceneManager
    {
        get { return _sceneManager; }
    }

    private void Awake()
    {
        //Init Singleton Instance
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        //Keep this objects including all its sub-game objects 
        DontDestroyOnLoad(this.gameObject);
    }

    public void AddScore(int score)
    {
        if(score >= 0)
            _overallScore += score;
    }

    public void InitGameOver()
    {
        _isGameOver = true;
    }

    public void ResetGame()
    {
        _isGameOver = false;
        _overallScore = 0;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
