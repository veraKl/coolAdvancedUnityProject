﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Player;

public class PlayerParent
{
   private Movement _movementObject;
   private Respawn _respawnObject;
   private Health _healthObject;

   public Movement Movement
   { 
      get { return _movementObject; }
   }

   public Respawn Respawn
   { 
      get { return _respawnObject; }
   }
   public Health Health
   { 
      get { return _healthObject; }
   }

   public PlayerParent()
   {
      _movementObject = GameObject.FindWithTag("Player").GetComponent<Movement>();
      _respawnObject = GameObject.FindWithTag("Player").GetComponent<Respawn>();
      _healthObject = GameObject.FindWithTag("Player").GetComponent<Health>();
   }
   
}
