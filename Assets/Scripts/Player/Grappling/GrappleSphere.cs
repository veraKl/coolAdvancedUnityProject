﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class GrappleSphere: MonoBehaviour
{
    List<GameObject>_internalPoints = new List<GameObject>();

    [SerializeField] private Material _outOfRangeMat;
    [SerializeField] private Material _inRangeMat;

    public List<GameObject> get_internalPoints()
    {
        return _internalPoints;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer==LayerMask.NameToLayer("Grappleable"))
        {
            _internalPoints.Add(other.gameObject);
            other.gameObject.GetComponent<Renderer>().material = _inRangeMat;
            Debug.Log(other+" other has entered the sphere");
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer==LayerMask.NameToLayer("Grappleable"))
        {
            _internalPoints.Remove(other.gameObject);
            other.gameObject.GetComponent<Renderer>().material = _outOfRangeMat;
            Debug.Log(other+" other has left the sphere");
        }
    }
}
