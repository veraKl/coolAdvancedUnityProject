﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;
using Object = UnityEngine.Object;

public class Grappling : MonoBehaviour
{
    private LineRenderer _lineRenderer;
    private Vector3 _grapplePoint;
    [SerializeField] private GameObject _muendung;
    public LayerMask _grappleableMask;
    public GrappleSphere _grappleSphere;
    private SpringJoint _joint;
    [SerializeField] private Transform _player;
    [SerializeField] private Animator _playerAnimator;

    public Animator PlayerAnimator
    {
        get { return _playerAnimator; }
    }
    
    private void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        
        //this would be better than making the connection using the serialized field
        //_player = GameManager.Instance.PlayerParent;

    }

    void Update()
    {
        if (!PauseManager.GameIsPaused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartGrapple();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                StopGrapple();
            }
        }
    }

    private void LateUpdate()
    {
        DrawRope();
    }

    private void StartGrapple()
    {
        //make game unpausable during grappling
        //GameManager.Instance.UIManager.Escapable = false;
        
        
        List<GameObject> points = _grappleSphere.get_internalPoints();
        
        Debug.Log(String.Format("Grapple Points detected: {0}",points.Count));
        //if there are no points to grapple to
        if (points.Count ==0) return;

        _lineRenderer.enabled = true;
        _lineRenderer.useWorldSpace = true;
        _playerAnimator.SetBool("isGrappling", true);
        
        //select the point to grapple to
        GameObject closest = points[0];
        if (points.Count > 1)
        {
            for (int i = 1; i < points.Count; i++)
            {
                if (Vector3.Distance(points[i].transform.position, _muendung.transform.position) <
                    Vector3.Distance(closest.transform.position, _muendung.transform.position))
                {
                    closest = points[i];
                }
            }
        }
        
        _grapplePoint = closest.transform.position;
        _joint = _player.gameObject.AddComponent<SpringJoint>();
        _joint.autoConfigureConnectedAnchor = false;
        _joint.connectedAnchor = _grapplePoint;
        //set the maximum/minimum distance between the Object and the _muendung to the current max. Distance
        float distanceFromPoint = Vector3.Distance(_muendung.transform.position, _grapplePoint);
        _joint.maxDistance = distanceFromPoint*0.8f;
        _joint.minDistance = distanceFromPoint*0.2f;
        
        //Configure however you think it fits
        _joint.spring = 4.5f * 80f; //How much Pull/Push the joint has
        _joint.damper = 7f * 80f;
        _joint.massScale = 4.5f * 80f;

        _lineRenderer.positionCount = 2;
    }

    void DrawRope()
    {
    //dont draw when we are not grappling
    if (!_joint) return;
    
    _lineRenderer.SetPosition(0, _muendung.transform.position);
    _lineRenderer.SetPosition(1, _grapplePoint);
    }

    private void StopGrapple()
    {
        //Debug.Log("StopGrapple()");
        _lineRenderer.positionCount = 0;
        Destroy(_joint);
        
        _playerAnimator.SetBool("isGrappling", false);
        
        //GameManager.Instance.UIManager.Escapable = true;

        _lineRenderer.useWorldSpace = false;
        _lineRenderer.enabled = false;
    }


}
