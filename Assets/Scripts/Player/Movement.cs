﻿using System;
using System.Collections;
using System.Net.Sockets;
using Cinemachine;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Player
{
    [RequireComponent(typeof(Rigidbody))]
    public class Movement : MonoBehaviour
    {

        #region Fields: Private Properties
        
        //some components of the player
        private Rigidbody _playerBody;
        private Collider _playerCollider;
        [SerializeField] private Animator _playerAnimator;

        [SerializeField] 
        private float _defaultForceMultiplier = 1.5f;
        [SerializeField]
        private float _jumpForceMultiplier = 80f;
        [SerializeField]
        private float _slopeWalkingForceMultiplier = 0.25f;
        
        private float _nextAllowedZoomTime = 0f;
        private float _nextAllowedZoomTimeDelay = 1f;

        //the layer that the players considers as solid
        [SerializeField]private LayerMask _groundLayer;
    
        //the attributes of the player
        [SerializeField] private float _speed;
        [SerializeField] private float _groundMovementModifier=1f;
        [SerializeField] private float _airMovementModifier=0.2f;
        [SerializeField] private float _jumpHeight;

        [Header("Drag")]
        private float _groundDrag = 0.5f;
        private float _airDrag = 0.18f;
        
        //the direction the player currently moves in
        private Vector3 _inputVector;
        
        //free look camera
        [SerializeField] private Transform _cam;
        [SerializeField] private CinemachineFreeLook _camBrain;
        
        //player rotation smooth timer
        private float _turnSmoothTime = 0.1f;
        //player rotation velocity
        private float _turnSmoothVelocity;
        //private Vector3 _moveDirection = Vector3.forward;
        
        //Check for movement status
        [SerializeField]
        private bool _isClimbing;

        private float _verticalClimbSpeed = 1.5f;
        private float _horizontalClimbSpeed = 2f;
        [SerializeField]
        private bool _isGrounded;
        [SerializeField] 
        private bool _isSliding;

        private Vector3 _slideDir;

        private float _slideTimer;
        private float _slideTimerMax = 1.5f;
        
        //cam FoV settings
        private float _targetFoV;
        private float _maxFoVChange = 15f;
        

        private RaycastHit _hit;
        private RaycastHit _slopeHit;
        
        private Vector3 _moveDir = Vector3.forward;
        private Vector3 _slopeMoveDir;

        private float _jumpTimer = 0f;
        private bool _allow2ndJump = true;
        private float _pushForce = 4f;
        private BoxCollider _boxCollider;
        
        #endregion

        #region Fields: Public Properties

        public CinemachineFreeLook CamBrain
        {
            get { return _camBrain; }
        }

        public Rigidbody PlayerBody
        {
            get
            {
                return _playerBody;
            }
        }

        #endregion
        

        // Start is called before the first frame update
        void Start()
        {
            _boxCollider = GetComponent<BoxCollider>();
            _playerBody = GetComponent<Rigidbody>();
            _playerCollider = GetComponent<Collider>();

            _speed = 8f;
        }

        private void Update()
        {
            //checks if the player is grounded or not. Todo: Check if it is neccessary to check the ground-layer
            //_isGrounded = Physics.Raycast(transform.position, Vector3.down, _playerCollider.bounds.size.y/2+0.1f);
            //_isGrounded = Physics.Raycast(transform.position, Vector3.down, _playerCollider.bounds.size.y/2+0.1f, _groundLayer);
            _isGrounded = Physics.CheckSphere(transform.position - new Vector3(0, 0.1f, 0), 0.5f, _groundLayer);
            ControllDrag();
            //Player Respawn Key
            if (Input.GetKeyDown(KeyCode.R))
            {
                GameManager.Instance.PlayerParent.Respawn.PlayerRespawn();
            }
            
            #region Region: Animations & Jumping
            if (!_isGrounded)
            {
                _playerAnimator.SetBool("isOnGround",false);
                //double jump
                if (_allow2ndJump && Input.GetKeyDown(KeyCode.Space))
                {
                    _playerBody.AddForce(new Vector3(0,_jumpHeight * _jumpForceMultiplier,0), ForceMode.Impulse);
                    _allow2ndJump = false;
                }
                    
            }
            else
            {
                _allow2ndJump = true;
                _playerAnimator.SetBool("isOnGround",true);
                if (Input.GetKeyDown(KeyCode.Space)  && Time.time-_jumpTimer>0.02f)
                {
                    _jumpTimer = Time.time;
                    //inform the animator, that we are now jumping
                    _playerAnimator.SetTrigger("onJump");
                    //set the animation-set to grounded
                    _playerAnimator.SetBool("isOnGround", false);


                    _playerBody.AddForce(new Vector3(0,_jumpHeight * _jumpForceMultiplier,0), ForceMode.Impulse);
                }
            }
            #endregion
            
            if (Input.GetKeyDown(KeyCode.LeftControl) && !_isSliding && _isGrounded)
            {
                _slideTimer = 0.0f; // start timer
                _isSliding = true;
                _slideDir = _moveDir;
            }
            
        }

        private void OnDrawGizmosSelected()
        {   //vizuallizes if the playewr is grounded.
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.position - new Vector3(0, 0.1f, 0), 0.5f);
        }

        private void ControllDrag()
        {
            
            if (_isGrounded)
            {
                _playerBody.drag = _groundDrag;
            }
            else
            {
                _playerBody.drag = _airDrag;
            }
            _playerBody.drag = _groundDrag;

        }

        // FixedUpdate is called once per 50 Frames.
        private void FixedUpdate()
        {
            #region Region: Sliding
            
            
            if (_isSliding)
            {
                _playerAnimator.SetBool("isSliding",true);
                if (_slideTimer < _slideTimerMax)
                {
                    //reduce box collider size
                    _boxCollider.size = new Vector3(1.028049f, 0.5f, 0.71f);
                    if (OnSlope())
                    {
                        _playerBody.drag = 0f;
                        _playerBody.AddRelativeForce(_slideDir*_defaultForceMultiplier*20f, ForceMode.Impulse);
                    }
                    else
                    {
                        _playerBody.AddRelativeForce(new Vector3(0,0,0.4f) * _defaultForceMultiplier*20f, ForceMode.Impulse);
                        _playerBody.drag = 0.5f;
                    } 
                    _slideTimer += Time.deltaTime;
                }

                if (_slideTimer >= _slideTimerMax)
                {
                    _isSliding = false;
                    _playerAnimator.SetBool("isSliding",false);
                    _boxCollider.size = new Vector3(1.028049f, 1.835302f, 0.71f);
                    _playerBody.drag = _groundDrag;
                }
            }
            #endregion
            
            #region Region: Camera
            ResetCamera();
            ChangeFoVOnMouseWheel();

            //Todo: Remove When Building Game
            if (Input.GetKeyDown(KeyCode.T))
            {
                SetCameraFoV(CamFoVMode.ZoomOut);
            }
            #endregion

            #region Region: Default & Climb Movement
            //read player input on x and y axis
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            float verticalInput = Input.GetAxisRaw("Vertical");
            
            
            
            //normalize for diagonal movement
            if (!_isClimbing)
            {
                _moveDir = DefaultMovement(horizontalInput, verticalInput);
            }
            else
            {
                ClimbMovement(horizontalInput, verticalInput, _moveDir);
            }
            
            #endregion
            
            
            
            //set animation
            _playerAnimator.SetFloat("Velocity",_playerBody.velocity.magnitude/_speed);
            //Debug.Log("Velocity: "+_playerBody.velocity.magnitude/_speed);
            
            
            
            #region Region: Slope Walking
            _slopeMoveDir = Vector3.ProjectOnPlane(_moveDir, _slopeHit.normal);
            if (_isGrounded && OnSlope())
            {
                //Debug.Log(String.Format("Slope Movement Dir: {0}, OnSlope: {1}", _slopeMoveDir, OnSlope()));
                
                //apply only the "upwards" force while slope climbing, we use usual default movement force for x/z axis
                _slopeMoveDir.Set(0f ,_slopeMoveDir.y,  0f);
                Debug.DrawRay(transform.position, _slopeMoveDir * 4, Color.magenta);
                _playerBody.AddForce(_slopeMoveDir.normalized  * (_speed * 1.25f) * _slopeWalkingForceMultiplier, ForceMode.Acceleration);
            }
            
            #endregion
        }

        #region Methods: Movement
        private Vector3 DefaultMovement(float horizontalInput, float verticalInput)
        {
            //horizontal movement x(left, right), z (forward, back)
            Vector3 direction = new Vector3(horizontalInput, 0f, verticalInput).normalized;
            Vector3 moveDirection = direction;
            //Debug.Log(String.Format("Direction: {0}, Magnitude: {1}",moveDirection, direction.magnitude));
            if (direction.magnitude >= 0.1f)
            {
                
                //rotate player towards the direction hes moving
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _cam.eulerAngles.y;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref _turnSmoothVelocity,
                    _turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                //turn rotation into a direction by multiplying Vector3.forward
                moveDirection = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;


                float Modifier = 1;
                if (_isGrounded)
                {
                    Modifier = _groundMovementModifier;
                }
                else
                {
                    Modifier = _airMovementModifier;
                }

                //Debug.Log("Modifier: " + Modifier);
                _playerBody.AddForce(new Vector3(
                    moveDirection.x*(_speed*Modifier*_defaultForceMultiplier), 
                    0, 
                    moveDirection.z*(_speed*Modifier*_defaultForceMultiplier)), ForceMode.Acceleration);
                //Debug.Log("Velocity:"+_playerBody.velocity);

                //init climbing detection
                InitMovement(moveDirection);
                
            }
            return moveDirection;
        }
        
        #region Methods: Climbing
        
        private void ClimbMovement(float horizontalInput, float verticalInput, Vector3 moveDirection)
        {
            //vertical movement x(left,right) ,y (forward,back)
            SetCameraFoV(CamFoVMode.ZoomOut);
           
           

            //Vector3 direction = new Vector3(-horizontalInput, verticalInput, 0f).normalized;
            float climbHorizontalFactor = 0.15f;
           
            Vector3 direction = new Vector3(transform.right.x * horizontalInput * climbHorizontalFactor, 
                verticalInput, 
                transform.right.z * horizontalInput * climbHorizontalFactor).normalized;

            _playerBody.velocity = 
                new Vector3(direction.normalized.x * _horizontalClimbSpeed, 
                            direction.normalized.y * _verticalClimbSpeed, 
                            direction.normalized.z * _horizontalClimbSpeed) ;

            InitMovement(moveDirection);
        }
        
        public void InitMovement(Vector3 forwardDir)
        {
            CheckForClimb(forwardDir);
        }

        public void CheckForClimb(Vector3 forwardDir) //check if the player is climbing
        {
            Vector3 origin = transform.position;
            Vector3 eyePosition = origin;
            eyePosition.y += 1.5f;
            Vector3 bellyPosition = origin;
            bellyPosition.y += 0.7f;
            //Vector3 dir = transform.forward;
            RaycastHit _hit = default;
            Debug.DrawRay(eyePosition + new Vector3(0,1.5f, 0),forwardDir, Color.green);
            Debug.DrawRay(bellyPosition + new Vector3(0,0.7f,0 ),forwardDir, Color.green);
            //Debug.Log("Raycast Direction: " + forwardDir);
            //if a object is hit activate climb mode (switch axis so that y upwards is forward)
            if (Physics.Raycast(eyePosition, forwardDir, out _hit, 0.5f) || 
                Physics.Raycast(bellyPosition, forwardDir, out _hit, 0.5f))
            {
                //get potential climb object
                GameObject climbObject = _hit.transform.gameObject;
                if(climbObject.CompareTag("Climbable"))
                {
                    //only climb if it´s marked climbable
                    _isClimbing = true;
                    //activate the climbing animation
                    _playerAnimator.SetBool("isClimbing", _isClimbing);
                }
            }
            else
            {
                if (_isClimbing)
                {
                    //delay climb mode deactivation to allow player to "get on the edge"
                    StartCoroutine("DelayedToggleOffClimbing");
                }
            }

            //Debug.Log("Climbing: "+  _isClimbing);
        }
        
        private IEnumerator DelayedToggleOffClimbing()
        {
            yield return new WaitForSeconds(1.5f);
            _isClimbing = false;
            
            //deactivate the climbing animation
            _playerAnimator.SetBool("isClimbing", _isClimbing);
            yield return new WaitForSeconds(1f);
            SetCameraFoV(CamFoVMode.Default);
        }
        
        #endregion
        private bool OnSlope()
        {
            Debug.DrawRay(transform.position,Vector3.down, Color.red);
            if (Physics.Raycast(transform.position, Vector3.down, out _slopeHit, 3f))
            {
                if (_slopeHit.normal != Vector3.up)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }
        
        #endregion

        #region Methods: Camera
        //Reset Camera on Key Down ("F")
        private void ResetCamera()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                //Todo: transition camera reset with an animation
                _camBrain.m_XAxis.Value = 0f;
                _camBrain.m_YAxis.Value = 0.15f;
                _camBrain.m_Lens.FieldOfView = (float) CamFoVMode.Default;
            }
                
        }
        
        private void SetCameraFoV(CamFoVMode mode)
        {
            if (_nextAllowedZoomTime < Time.time)
            {
                _nextAllowedZoomTime = Time.time + _nextAllowedZoomTimeDelay;
                _targetFoV = (float) mode;
            }

            _camBrain.m_Lens.FieldOfView =
                Mathf.MoveTowards(_camBrain.m_Lens.FieldOfView, _targetFoV, _maxFoVChange * Time.deltaTime);
        }

        private void ChangeFoVOnMouseWheel()
        {
            if (Input.GetAxis("Mouse ScrollWheel") > 0f ) // forward
            {
                _camBrain.m_Lens.FieldOfView -= 0.25f;
            }
            else if (Input.GetAxis("Mouse ScrollWheel") < 0f ) // backwards
            {
                _camBrain.m_Lens.FieldOfView += 0.25f;
            }
        }
        
        //Todo: FoV zoom out when grabling hook
        
        #endregion
        
        #region Methods: Event Functions

        private void OnCollisionEnter(Collision other)
        {
            //Debug.Log(other.transform.name);
            if (other.transform.CompareTag("SlowPlain"))
            {
                //slow down player
                _speed = _speed * 3/4f;
            }
            else if (other.transform.CompareTag("SpeedUpPlain"))
            {
                _speed = _speed * 2;
            }
            else if (other.transform.CompareTag("SevereDanger"))
            {
                //Damage Player Lifes
            }
        }
        
         private void OnCollisionExit(Collision other)
        {
            //Debug.Log(other.transform.name);
            if (other.transform.CompareTag("SlowPlain"))
            {
                //return to old speed
                _speed = _speed * 4/3f;
            }else if (other.transform.CompareTag("SpeedUpPlain"))
            {
                StartCoroutine("DelayedResetSpeed");
            }
        }
        #endregion
        
        private IEnumerator DelayedResetSpeed()
        {
            //boost lasts 1s
            yield return new WaitForSeconds(1f);
            _speed = _speed * 1/2f;
        }
        
    }
}
