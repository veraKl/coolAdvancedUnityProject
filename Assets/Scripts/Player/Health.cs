﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    private const int _MAX_POINTS = 1000;

    //player starts with 100 points on each level (will be set seperately)
    private int _points = 100;

    //ZeroHealth Event
    public delegate void ZeroHealthDelegate();
    public event ZeroHealthDelegate OnZeroHealth;

    public int Points
    {
        get { return _points;}
    }

    private void Start()
    {
        //register Event listeners
        OnZeroHealth += GameManager.Instance.OnGameOver;
    }

    public void AddPoints(int amount)
    {
        if(_points < _MAX_POINTS)
            _points += amount;
        else
        {
            _points = _MAX_POINTS;
        }
    }

    public void RemovePoints(int amount)
    {
        if (_points - amount > 0)
        {
            _points -= amount;
        }
        //notify GameManager via Parent Object about 0 lives
        else
        {
            //there are listeners, fire OnZeroHealth event
            if(OnZeroHealth != null)
                OnZeroHealth.Invoke();
            _points = 0;
        }

    }

    public void ResetPoints(int newStartPoints)
    {
        _points = newStartPoints;
    }
}
