﻿using UnityEngine;

namespace Player
{
    public class Respawn : MonoBehaviour
    {
        [SerializeField] private SpawnPoint _spawnPoint;
    
    
        // Start is called before the first frame update
        void Start()
        {
            //spawn at the first spawnpoint
            this.transform.position = _spawnPoint.transform.position;
        }
    

        //setting the current spawnpoint to this location
        public void SetSpawnpoint(SpawnPoint sp)
        {
            //deactivate old Spawnpoint
            _spawnPoint.SetIsCurrent(false);
            _spawnPoint = sp;
        }

        
        //Respawn the player at the position of this Spawnpoint.
        public void PlayerRespawn()
        {
            Debug.Log("Just respawned");
            this.transform.position = _spawnPoint.transform.position;
        }

    }
}
