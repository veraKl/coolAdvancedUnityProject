﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CamDefaultSettings
{
    public static readonly CamFoVMode CamFoVMode = CamFoVMode.Default;
    public static readonly float SensivityX = 300f;
    public static readonly float SensivityY = 0.25f;
} 
