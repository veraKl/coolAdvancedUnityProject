﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class CamTrackManager : MonoBehaviour
{
    [SerializeField] private GameObject _mainGameCamObject;
    [SerializeField] private GameObject _introCamTrackObject;
    [SerializeField] private GameObject _activeGameGUI;

    private CinemachineDollyCart _cart;

    private void Start()
    {
        if(_activeGameGUI.gameObject.activeInHierarchy)
            _activeGameGUI.gameObject.SetActive(false);
        if(_mainGameCamObject.gameObject.activeInHierarchy)
            _mainGameCamObject.gameObject.SetActive(false);
        if(!_introCamTrackObject.gameObject.activeInHierarchy)
            _introCamTrackObject.gameObject.SetActive(true);
        _cart = _introCamTrackObject.GetComponentInChildren<CinemachineDollyCart>();
    }

    private void Update()
    {
        if (_cart.m_PositionUnits == CinemachinePathBase.PositionUnits.Normalized &&
            _cart.m_Position == 1.0f)
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        _introCamTrackObject.gameObject.SetActive(false);
        _mainGameCamObject.gameObject.SetActive(true);
        _activeGameGUI.gameObject.SetActive(true); 
    }
}
