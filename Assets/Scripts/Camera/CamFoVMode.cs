﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CamFoVMode 
{
    Default = 40,
    ZoomOut = 80,
    ZoomIn = 20
}
